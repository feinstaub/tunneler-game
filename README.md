Tunneler Game
=============

The file README.orig contains the original README text.

Original website: http://users.jyu.fi/~tvkalvas/code/tunneler/

openSUSE package: https://build.opensuse.org/package/show/games/tunneler

This repository contains 1.1.2 and onwards
------------------------------------------
* Based on the original source code
* Includes additions from the openSUSE package
* Make it compile and run on current systems

Known Bugs
----------
* None

Solved bugs since version 1.1.1
-------------------------------
### Tunneler crashes at startup:

    tunneler: malloc.c:2392: sysmalloc: Assertion `(old_top == initial_top (av) && old_size == 0) || ((unsigned long) (old_size) >= MINSIZE && prev_inuse (old_top) && ((unsigned long) old_end & (pagesize - 1)) == 0)' failed.
    fish: “tunneler” terminated by signal SIGABRT (Abort)

See here: https://stackoverflow.com/questions/2987207/why-do-i-get-a-c-malloc-assertion-failure

Fixed with:

    cd src
    valgrind --leak-check=yes ./tunneler
